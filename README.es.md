# Un agregado para generar sitios transportables en Jekyll

Este agregado hace dos cosas:

* Crea una etiqueta Liquid llamada `{% base %}` para que las plantillas
  puedan encontrar su posición relativa con respecto a la raíz del
  sitio.

* Elimina la raíz (la primera `/`) de las URLs para volverlas relativas.

La combinación de ambos te permite generar sitios que funcionan sin
cambios en cualquier ubicación, incluso en uno o varios niveles de
sub-directorios, volviéndolos transportables :)

## Ejemplos de uso

Cuando habilitas este agregado sumándolo a la lista de `plugins` en tu
`_config.yml`, todas las URLs generadas por Jekyll (como las URLs de los
artículos) van a ser relativas a la raíz del sitio (donde sea que esté).

Para las otras URLs que uses en tus plantillas, como archivos Javascript
y CSS, vas a necesitar eliminar la primera `/` en todos lados.  Cuando
eliminas esta primera parte de la ruta de una URL, estás indicando al
navegador que son relativas a la URL actual.

### Como la etiqueta base (cambios mínimos)

Puedes agregar la meta-etiqueta `<base>` al `<head>` de tu plantilla.
El navegador va a traducir cada URL relativa en relación a eso en lugar
de a la URL actual.

```html
<head>
  <base href="{% base %}"/>
</head>
```

Esto tiene un [pequeño
problema](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base#Anchor_elements)
(en inglés, la traducción no contiene la nota).  Si estás usando
direcciones `#identificador` para la navegación interna, el navegador
también agrega la URL base a estas direcciones.  Hay que tener cuidado
con agregar la URL actual antes que los `#identificador`.

Así que esto

```html
<a href="#identificador">Saltar dentro de la misma página</a>
```

Se convierte en

```html
<a href="{{ page.url }}#anchor">Saltar dentro de la misma página</a>
```

### Como prefijo de cada URL

Si en cambio quieres tomar la ruta larga (!) aunque evitando el problema
con los `#identificador`, puedes prefijar cada URL dentro de tu
plantilla o artículo con la etiqueta `{% base %}`.

```html
<a href="{% base %}{{ page.url }}">{{ page.title }}</a>
```

## Agradecimientos

* ["Relative paths in
  Jekyll"](https://ricostacruz.com/til/relative-paths-in-jekyll) por Rico
  Sta. Cruz

## Licencia

(Esta es una traducción no oficial del aviso de licencia publicado por
GNU.)

Copyright (c) 2019 Sutty <hi@sutty.nl>

jekyll-relative-urls es software libre: puedes redistribuirlo y/o
modificarlo bajo los términos de la Licencia Pública General de GNU
publicada por la Free Software Foundation, en su versión 3 o cualquier
versión posterior.

jekyll-relative-urls se distribuye con la esperanza de resultar útil,
aunque SIN NINGUNA GARANTÍA; tampoco la garantía implícita de
COMERCIABILIDAD o ADECUACIÓN PARA UN PROPÓSITO PARTICULAR.  Lee la
Licencia Pública General de GNU para más detalles.

Deberías haber recibido una copia de la Licencia Pública General de GNU
junto con jekyll-relative-urls.  Si no, visita
<https://www.gnu.org/licenses/>.
