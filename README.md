# A jekyll plugin for transportable sites

This plugin does two things:

* It creates a `{% base %}` Liquid tag for templates to find their
  relative position to the site's root.

* It removes the root (the first `/`) from URLs to make them relative.

The combination of the two allows you to generate sites that work
without changes even if you put them on one or several levels of
subdirectories, thus making them transportable :)

## Sample usage

When you enable the plugin by adding it to the `plugins` array in your
`_config.yml`, every Jekyll-generated URL (like post URLs) will be made
relative to the site root (wherever that is).

For other URLs you put on your templates, like javascript or CSS files,
you'll need to remove the first `/` everywhere.  When you remove this
first part in the path of a URL, you're indicating they're relative to
the current URL.

### As base tag (minimal changes)

You can add the `<base>` metatag to the `<head>`.  The browser will
translate every relative URL in relation to this instead of the current
URL.

```html
<head>
  <base href="{% base %}"/>
</head>
```

This has it's
[gotcha](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base#Anchor_elements),
if you're using anchors for navigation, any `#anchor` is resolved to the
base URL, so you need to be careful to prepend the URL.

So, this

```html
<a href="#anchor">Jump inside the same page</a>
```

Becomes

```html
<a href="{{ page.url }}#anchor">Jump inside the same page</a>
```

### As URL prefix

Instead, if you want to go the long path (!) but avoid running into the
anchor gotcha, just prefix any URL on your template or article with the
`{% base %}` tag.

```html
<a href="{% base %}{{ page.url }}">{{ page.title }}</a>
```

## Thanks to

* ["Relative paths in
  Jekyll"](https://ricostacruz.com/til/relative-paths-in-jekyll) by Rico
  Sta. Cruz

## License

Copyright (c) 2019 Sutty <hi@sutty.nl>

jekyll-relative-urls is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

jekyll-relative-urls is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with jekyll-relative-urls.  If not, see <https://www.gnu.org/licenses/>.
