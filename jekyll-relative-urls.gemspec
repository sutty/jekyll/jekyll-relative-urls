# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-relative-urls'
  spec.version       = '0.0.6'
  spec.authors       = %w[Sutty]
  spec.email         = %w[hi@sutty.nl]

  spec.summary       = 'It allows you to generate sites that work without changes even if you put them on one or several levels of subdirectories, thus making them transportable :)'
  spec.description   = 'A jekyll plugin for transportable sites'
  spec.homepage      = "https://0xacab.org/sutty/jekyll/#{spec.name}"
  spec.license       = 'GPL-3.0'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_dependency 'jekyll', '~> 4'
end
