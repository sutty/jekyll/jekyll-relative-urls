# Copyright (c) 2019 Sutty <hi@sutty.nl>
#
# This file is part of jekyll-relative-urls
#
# jekyll-relative-urls is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# jekyll-relative-urls is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with jekyll-relative-urls.  If not, see
# <https://www.gnu.org/licenses/>.

# Asegurarse que Jekyll::Renderer exista antes de redefinirlo
require 'jekyll/renderer'

module Jekyll
  # Redefinir Renderer.render_document()
  class Renderer
    alias original_render_document render_document

    def render_document
      relativize_anchors_to original_render_document, document.url
    end

    private

    # Los links internos necesitan colocarse dentro de la misma URL o
    # sino volvemos al inicio del sitio, exceptuando la raíz del sitio.
    def relativize_anchors_to(input, base)
      return input if base == '/'

      input.gsub(/(href=["'])#/, "\\1#{base}#")
    end
  end
end
