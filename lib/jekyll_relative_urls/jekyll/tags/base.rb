# Copyright (c) 2019 Sutty <hi@sutty.nl>
#
# This file is part of jekyll-relative-urls
#
# jekyll-relative-urls is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# jekyll-relative-urls is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with jekyll-relative-urls.  If not, see
# <https://www.gnu.org/licenses/>.

module Jekyll
  module Tags
    # Agrega una etiqueta Liquid {% base %} para obtener la posición
    # actual de una URL con respecto a la raíz del sitio.
    class Base < Liquid::Tag
      def render(context)
        # Obtiene la cantidad de directorios en una URL
        depth = context.registers[:page]['url'].split('/').size
        # La ruta relativa a la raíz del sitio
        base  = ''

        # Si la ruta contiene extensión, descontar uno
        depth -= 1 unless context.registers[:page]['url'].end_with? '/'

        # Establecer el nivel en el que estamos, subiendo niveles hacia la
        # raíz del sitio
        depth.times do
          base += '../'
        end

        # Devolver la ruta relativa a la raíz del sitio
        base
      end
    end
  end
end

Liquid::Template.register_tag('base', Jekyll::Tags::Base)
