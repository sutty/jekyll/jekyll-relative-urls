# Copyright (c) 2019 Sutty <hi@sutty.nl>
#
# This file is part of jekyll-relative-urls
#
# jekyll-relative-urls is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# jekyll-relative-urls is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with jekyll-relative-urls.  If not, see
# <https://www.gnu.org/licenses/>.

# Asegurarse que Jekyll::URL exista antes de redefinirla
require 'jekyll/url'

module Jekyll
  # Redefine la clase Jekyll::URL
  class URL
    # Eliminar la raíz de todas las URLs salvo en la raíz misma
    def to_s
      # Igual que en jekyll-4.0.0/lib/jekyll/url.rb
      url = sanitize_url(generated_permalink || generated_url)
      url.gsub!(%r{^/}, '') unless url.length == 1

      url
    end
  end
end
